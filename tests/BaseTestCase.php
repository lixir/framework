<?php

namespace Lixir\Tests;

use Faker\Generator as FakerGenerator;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\Facades\Notification;
use Lixir\Database\Contracts\CurrencyModelInterface;
use Lixir\Database\Models\AdminUser;
use Lixir\Database\Models\Currency;
use Lixir\LixirServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

abstract class BaseTestCase extends OrchestraTestCase
{
    protected $user;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = $this->app->make(FakerGenerator::class);
        $this->withFactories(__DIR__ . '/../database/factories');

        $this->setUpDatabase();
        $this->setDefaultCurrency();

        Notification::fake();
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('app.key', 'base64:UTyp33UhGolgzCK5CJmT+hNHcA+dJyp3+oINtX+VoPI=');
    }

    private function resetDatabase(): void
    {
        $this->artisan('migrate');
    }

    protected function getPackageProviders($app): array
    {
        return [
            LixirServiceProvider::class,
        ];
    }

    protected function setUpDatabase(): void
    {
        $this->loadLaravelMigrations();
        $this->resetDatabase();
    }

    protected function setDefaultCurrency()
    {
        factory(Currency::class)->create();
        $currencyRepository = app(CurrencyModelInterface::class);
        $currency = $currencyRepository->all()->first();
        $this->withSession(['default_currency' => $currency]);
    }

    protected function getPackageAliases($app): array
    {
        return [
            //'AdminMenu' => 'Lixir\\AdminMenu\\Facade',
            //'AdminConfiguration' => 'Lixir\\AdminConfiguration\\Facade',
            //'DataGrid' => 'Lixir\\DataGrid\\Facade',
            //'Image' => 'Lixir\\Image\\Facade',
            'Breadcrumb' => \Lixir\Support\Facades\Breadcrumb::class,
            'Menu' => \Lixir\Support\Facades\Menu::class,
            'Module' => \Lixir\Support\Facades\Module::class,
            'Permission' => \Lixir\Support\Facades\Permission::class,
            'Cart' => Lixir\Support\Facades\Cart::class,
            'Payment' => Lixir\Support\Facades\Payment::class,
            'Shipping' => Lixir\Support\Facades\Shipping::class,
            'Tab' => Lixir\Support\Facades\Tab::class,
            //'Theme' => 'Lixir\\Theme\\Facade',
            'Widget' => Lixir\Support\Facades\Widget::class
        ];
    }

    protected function createAdminUser($data = ['is_super_admin' => 1]): self
    {
        if (null === $this->user) {
            $this->user = factory(AdminUser::class)->create($data);
        }

        return $this;
    }
}
