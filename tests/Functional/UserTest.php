<?php

namespace Lixir\Tests\Functional;

use Lixir\System\Controllers\LoginController;
use Lixir\Tests\BaseTestCase;

/** @runInSeparateProcess */
class UserTest extends BaseTestCase
{
    public function testRedirectPathForLoginController()
    {
        $loginController = app(LoginController::class);
        $adminPath = config('lixir.admin_url');
        $this->assertEquals($loginController->redirectPath(), $adminPath);
    }
}
