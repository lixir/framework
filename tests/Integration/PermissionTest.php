<?php

namespace Lixir\Tests\Integration;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Lixir\Database\Models\AdminUser;
use Lixir\Database\Models\Permission;
use Lixir\Tests\BaseTestCase;

class PermissionTest extends BaseTestCase
{
    use RefreshDatabase;

    public function testPermission()
    {
        $this->markTestIncomplete('Todo Fix this unit test');
        $this->createAdminUser(['is_super_admin' => 0])
            ->actingAs($this->user, 'admin')
            ->get(route('admin.dashboard'))
            ->assertStatus(Response::HTTP_FORBIDDEN);

        $this->createPermissionForUser($this->user, 'admin.dashboard');
        $this->actingAs($this->user, 'admin')
            ->get(route('admin.dashboard'))
            ->assertStatus(Response::HTTP_OK);
    }

    protected function createPermissionForUser(AdminUser $user, string $name)
    {
        $permission = new Permission(['name' => $name]);
        $user->role->permissions()->save($permission);
        $user->load('role.permissions');
    }
}
