# Core Package for Lixir E-Commerce (A Laravel Shopping Cart)
Lixir Framework contains the core features for the Lixir E-Commerce. It is based behind the Laravel E-Commerce.

## Requirements

## Installation
This framework is provided as a composer package.

    composer require lixir/framework

Once this is done, you will need a few commands to run to finish up the installation.

## Publishing
Publish the Lixir E-Commerce Framework config file and assets (JS/CSS and images):

    php artisan vendor:publish --provider="Lixir\LixirServiceProvider"

Once done, we need to run a command to install the required database tables:

    php artisan lixir:install

Now, create your Administrator account:

    php artisan lixir:admin:make

And that's it. You can now visit your site:

    yoursite.com/admin
