<?php

namespace Lixir\Catalog\Controllers;

use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\Category;
use Lixir\Catalog\Requests\CategoryRequest;
use Lixir\Database\Contracts\CategoryModelInterface;

class CategoryController
{
    /**
     * Category Repository for the Install Command.
     * @var \Lixir\Database\Repository\CategoryRepository
     */
    protected $categoryRepository;

    /**
     * Construct for the lixir install command.
     * @param \Lixir\Database\Contracts\CategoryModelInterface $categoryRepository
     */
    public function __construct(
        CategoryModelInterface $categoryRepository
    ) {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Show Category Index Page.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $categories = $this->categoryRepository->all();

        return view('lixir::catalog.category.index')
            ->with(compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('catalog.category');

        return view('lixir::catalog.category.create')
            ->with(compact('tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\Catalog\Requests\CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        $this->categoryRepository->create($request->all());

        return redirect()->route('admin.category.index')
            ->with('successNotification', __(
                'lixir::system.notification.store',
                ['attribute' => __('lixir::catalog.category.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\Category $category
     * @return \Illuminate\View\View
     */
    public function edit(Category $category)
    {
        $tabs = Tab::get('catalog.category');

        return view('lixir::catalog.category.edit')
            ->with(compact('category', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\Catalog\Requests\CategoryRequest $request
     * @param \Lixir\Database\Models\Category  $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->update($request->all());

        return redirect()->route('admin.category.index')
            ->with('successNotification', __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::catalog.category.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\Category  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'lixir::system.notification.delete',
                ['attribute' => __('lixir::catalog.category.title')]
            ),
        ]);
    }
}
