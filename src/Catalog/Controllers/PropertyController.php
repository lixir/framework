<?php

namespace Lixir\Catalog\Controllers;

use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\Property;
use Lixir\Catalog\Requests\PropertyRequest;
use Lixir\Database\Contracts\PropertyModelInterface;

class PropertyController
{
    /**
     * Property Repository for the Property Controller.
     * @var \Lixir\Database\Repository\PropertyRepository
     */
    protected $propertyRepository;

    /**
     * Construct for the lixir property controller.
     * @param \Lixir\Database\Contracts\PropertyModelInterface $propertyRepository
     */
    public function __construct(
        PropertyModelInterface $propertyRepository
    ) {
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * Show Dashboard of an lixir Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $properties = $this->propertyRepository->all();

        return view('lixir::catalog.property.index')
            ->with(compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('catalog.property');
        $dataTypeOptions = Property::PROPERTY_DATATYPES;
        $fieldTypeOptions = Property::PROPERTY_FIELDTYPES;

        return view('lixir::catalog.property.create')
            ->with(compact('dataTypeOptions', 'fieldTypeOptions', 'tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\Catalog\Requests\PropertyRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PropertyRequest $request)
    {
        $property = $this->propertyRepository->create($request->all());
        $this->savePropertyDropdownOptions($property, $request);

        return redirect()->route('admin.property.index')
            ->with('successNotification', __(
                'lixir::system.notification.store',
                ['attribute' => __('lixir::catalog.property.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\Property $property
     * @return \Illuminate\View\View
     */
    public function edit(Property $property)
    {
        $tabs = Tab::get('catalog.property');
        $dataTypeOptions = Property::PROPERTY_DATATYPES;
        $fieldTypeOptions = Property::PROPERTY_FIELDTYPES;

        return view('lixir::catalog.property.edit')
            ->with(compact('property', 'dataTypeOptions', 'fieldTypeOptions', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\Catalog\Requests\PropertyRequest $request
     * @param \Lixir\Database\Models\Property  $property
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PropertyRequest $request, Property $property)
    {
        $property->update($request->all());
        $this->savePropertyDropdownOptions($property, $request);

        return redirect()->route('admin.property.index')
            ->with('successNotification', __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::catalog.property.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\Property  $property
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Property $property)
    {
        $property->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'lixir::system.notification.delete',
                ['attribute' => __('lixir::catalog.property.title')]
            ),
        ]);
    }

    /**
     * Save Property Dropdown options.
     * @param \\Lixir\Database\Models\Property  $property
     * @param \Lixir\Catalog\Requests\PropertyRequest $request
     * @return void
     */
    private function savePropertyDropdownOptions(Property $property, PropertyRequest $request)
    {
        if (! ($request->get('field_type') === 'RADIO' || $request->get('field_type') === 'SELECT')) {
            $property->dropdownOptions()->delete();
        }
        if (($request->get('field_type') === 'RADIO' ||
            $request->get('field_type') === 'SELECT') &&
            count($request->get('dropdown_option')) > 0
        ) {
            foreach ($request->get('dropdown_option') as $key => $option) {
                if (empty($option)) {
                    continue;
                }

                if (is_string($key)) {
                    $property->dropdownOptions()->create(['display_text' => $option]);
                } else {
                    $optionModel = $property->dropdownOptions()->find($key);
                    $optionModel->update(['display_text' => $option]);
                }
            }
        }
    }
}
