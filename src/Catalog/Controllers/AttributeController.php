<?php

namespace Lixir\Catalog\Controllers;

use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\Attribute;
use Lixir\Catalog\Requests\AttributeRequest;
use Lixir\Catalog\Requests\AttributeImageRequest;
use Lixir\Database\Contracts\AttributeModelInterface;

class AttributeController
{
    /**
     * Attribute Repository for the Attribute Controller.
     * @var \Lixir\Database\Repository\AttributeRepository
     */
    protected $attributeRepository;

    /**
     * Construct for the lixir install command.
     * @param \Lixir\Database\Contracts\AttributeModelInterface $attributeRepository
     */
    public function __construct(
        AttributeModelInterface $attributeRepository
    ) {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Show Dashboard of an lixir Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $attributes = $this->attributeRepository->all();

        return view('lixir::catalog.attribute.index')
            ->with(compact('attributes'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('catalog.attribute');
        $displayAsOptions = Attribute::DISPLAY_AS;

        return view('lixir::catalog.attribute.create')
            ->with(compact('displayAsOptions', 'tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\Catalog\Requests\AttributeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AttributeRequest $request)
    {
        $attribute = $this->attributeRepository->create($request->all());
        $this->saveAttributeDropdownOptions($attribute, $request);

        return redirect()->route('admin.attribute.index')
            ->with('successNotification', __(
                'lixir::system.notification.store',
                ['attribute' => __('lixir::catalog.attribute.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\Attribute $attribute
     * @return \Illuminate\View\View
     */
    public function edit(Attribute $attribute)
    {
        $tabs = Tab::get('catalog.attribute');
        $displayAsOptions = Attribute::DISPLAY_AS;

        return view('lixir::catalog.attribute.edit')
            ->with(compact('attribute', 'displayAsOptions', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\Catalog\Requests\AttributeRequest $request
     * @param \Lixir\Database\Models\Attribute  $attribute
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AttributeRequest $request, Attribute $attribute)
    {
        $attribute->update($request->all());
        $this->saveAttributeDropdownOptions($attribute, $request);

        return redirect()->route('admin.attribute.index')
            ->with('successNotification', __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::catalog.attribute.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\Attribute  $attribute
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Attribute $attribute)
    {
        $attribute->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'lixir::system.notification.delete',
                ['attribute' => __('lixir::catalog.attribute.title')]
            ),
        ]);
    }

    /**
     * Save Attribute Dropdown options.
     * @param \\Lixir\Database\Models\Attribute  $attribute
     * @param \Lixir\Catalog\Requests\AttributeRequest $request
     * @return void
     */
    public function saveAttributeDropdownOptions(Attribute $property, AttributeRequest $request)
    {
        if ($request->get('dropdown_option') !== null && count($request->get('dropdown_option')) > 0) {
            foreach ($request->get('dropdown_option') as $key => $option) {
                if (empty($option)) {
                    continue;
                }

                $optionModel = $property->dropdownOptions()->find($key);

                if ($optionModel === null) {
                    $property->dropdownOptions()->create($option);
                } else {
                    $optionModel->update($option);
                }
            }
        }
    }

    /**
     * upload user image to file system.
     * @param \Lixir\System\Requests\AdminUserImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(AttributeImageRequest $request)
    {
        $image = $request->file('dropdown_options_image');
        $path = $image->store('uploads/catalog/attributes', 'public');

        return response()->json([
            'success' => true,
            'path' => $path,
            'message' => __('lixir::system.notification.upload', ['attribute' => 'Attribute Image']),
        ]);
    }
}
