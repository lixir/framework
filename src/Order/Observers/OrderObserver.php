<?php

namespace Lixir\Order\Observers;

use Lixir\Database\Contracts\ConfigurationModelInterface;
use Lixir\Database\Models\Order;
use Lixir\Widget\TotalOrder;

class OrderObserver
{
    /**
     * Handle the Order "created" event
     * @param \Lixir\Database\Models\Order $order
     * @return void
     */
    public function created(Order $order)
    {
        //
    }
}
