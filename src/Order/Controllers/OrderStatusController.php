<?php

namespace Lixir\Order\Controllers;

use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\OrderStatus;
use Lixir\Order\Requests\OrderStatusRequest;
use Lixir\Database\Contracts\OrderStatusModelInterface;

class OrderStatusController
{
    /**
     * OrderStatus Repository for the Install Command.
     * @var \Lixir\Database\Repository\OrderStatusRepository
     */
    protected $orderStatusRepository;

    /**
     * Construct for the lixir install command.
     * @param \Lixir\Database\Contracts\OrderModelInterface $orderStatusRepository
     */
    public function __construct(
        OrderStatusModelInterface $orderStatusRepository
    ) {
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * Show Dashboard of an lixir Admin.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderStatus = $this->orderStatusRepository->all();

        return view('lixir::order.order-status.index')
            ->with(compact('orderStatus'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tabs = Tab::get('order.order-status');

        return view('lixir::order.order-status.create')
            ->with(compact('tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\Order\Requests\OrderStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderStatusRequest $request)
    {
        $this->orderStatusRepository->create($request->all());

        return redirect()->route('admin.order-status.index')
            ->with('successNotification', __(
                'lixir::system.notification.store',
                ['attribute' => __('lixir::order.order-status.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\OrderStatus $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderStatus $orderStatus)
    {
        $tabs = Tab::get('order.order-status');

        return view('lixir::order.order-status.edit')
            ->with(compact('orderStatus', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\Order\Requests\OrderStatusRequest $request
     * @param \Lixir\Database\Models\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function update(OrderStatusRequest $request, OrderStatus $orderStatus)
    {
        $orderStatus->update($request->all());

        return redirect()->route('admin.order-status.index')
            ->with('successNotification', __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::order.order-status.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderStatus $orderStatus)
    {
        $orderStatus->delete();

        return [
            'success' => true,
            'message' => __(
                'lixir::system.notification.delete',
                ['attribute' => __('lixir::order.order-status.title')]
            ),
        ];
    }
}
