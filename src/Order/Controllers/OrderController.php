<?php

namespace Lixir\Order\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Response;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Lixir\Database\Models\Order;
use Lixir\Order\Mail\SentOrderInvoice;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Lixir\Order\Requests\OrderTrackCodeRequest;
use Lixir\Database\Contracts\OrderModelInterface;
use Lixir\Order\Requests\OrderChangeStatusRequest;
use Lixir\Database\Contracts\OrderStatusModelInterface;

class OrderController
{
    /**
     * Order Repository for the Install Command.
     * @var \Lixir\Database\Repository\OrderRepository
     */
    protected $orderRepository;

    /**
     * Order Repository for the Install Command.
     * @var \Lixir\Database\Repository\OrderStatusRepository
     */
    protected $orderStatusRepository;

    /**
     * Construct for the lixir install command.
     * @param \Lixir\Database\Contracts\OrderModelInterface $orderRepository
     */
    public function __construct(
        OrderModelInterface $orderRepository,
        OrderStatusModelInterface $orderStatusRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * Show Dashboard of an lixir Admin.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = $this->orderRepository->all();
        $orderStatuses = $this->orderStatusRepository->all();

        return view('lixir::order.order.index')
            ->with(compact('orderStatuses', 'orders'));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Order\Requests\OrderChangeStatusRequest  $request
     * @param \Lixir\Database\Models\Order  $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(OrderChangeStatusRequest $request, Order $order)
    {
        $order->update($request->all());

        return response()->json([
            'success' => true,
            'message' => __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::order.order.index.title')]
            ),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Order\Requests\OrderTrackCodeRequest  $request
     * @param \Lixir\Database\Models\Order  $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveTrackCode(OrderTrackCodeRequest $request, Order $order)
    {
        $order->update($request->all());

        return response()->json([
            'success' => true,
            'message' => __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::order.order.index.title')]
            ),
        ]);
    }

    /**
     * Show Order Details.
     * @param \Lixir\Database\Models\Order  $order
     * @return \Illuminate\View\View
     */
    public function show(Order $order): View
    {
        return view('lixir::order.order.show')
            ->with(compact('order'));
    }

    /**
     * Download Order Invoice in PDF.
     * @param \Lixir\Database\Models\Order  $order
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadInvoice(Order $order): BinaryFileResponse
    {
        $path = $this->generatePDF($order);

        return response()->download($path, 'invoice.pdf');
    }

    /**
     * Download Order Invoice in PDF.
     * @param \Lixir\Database\Models\Order  $order
     * @return  \Illuminate\Http\RedirectResponse
     */
    public function emailInvoice(Order $order)
    {
        $path = $this->generatePDF($order);
        $email = $order->user->email;

        Mail::to($email)
            ->send(
                new SentOrderInvoice($path)
            );

        return redirect()->route('admin.order.index');
    }

    /**
     * Generate PDF Invoice for the Given Order.
     * @param \Lixir\Database\Models\Order  $order
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function generateShippingLabel(Order $order): BinaryFileResponse
    {
        $folderPath = storage_path('app/public/uploads/orders');
        File::makeDirectory($folderPath, 0755, true, true);
        $path = $folderPath.'/shipping-label-'.$order->id.'.pdf';
        if (! File::exists($path)) {
            $html = view('lixir::order.order.shipping-label')
                ->with(compact('order'))
                ->render();

            PDF::loadHtml($html)
                ->setPaper('a4')
                ->save($path);
        }

        return response()->download($path, 'shipping-label.pdf');
    }

    /**
     * Generate PDF Invoice for the Given Order.
     * @param \Lixir\Database\Models\Order  $order
     * @return string
     */
    protected function generatePDF(Order $order): string
    {
        $folderPath = storage_path('app/public/uploads/orders');
        File::makeDirectory($folderPath, 0755, true, true);
        $path = $folderPath.'/invoice-'.$order->id.'.pdf';

        if (! File::exists($path)) {
            $html = view('lixir::order.order.invoice')
                ->with(compact('order'))
                ->render();

            PDF::loadHtml($html)
                ->setPaper('a4')
                ->save($path);
        }

        return $path;
    }
}
