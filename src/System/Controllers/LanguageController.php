<?php

namespace Lixir\System\Controllers;

use Illuminate\Routing\Controller;
use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\Language;
use Lixir\System\Requests\LanguageRequest;
use Lixir\Database\Contracts\LanguageModelInterface;

class LanguageController extends Controller
{
    /**
     * Language Repository for the Install Command.
     * @var \Lixir\Database\Repository\LanguageRepository
     */
    protected $languageRepository;

    /**
     * Construct for the lixir language controller.
     * @param \Lixir\Database\Contracts\LanguageModelInterface $languageRepository
     */
    public function __construct(
        LanguageModelInterface $languageRepository
    ) {
        $this->languageRepository = $languageRepository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $languages = $this->languageRepository->all();

        return view('lixir::system.language.index')
            ->with(compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('system.language');

        return view('lixir::system.language.create')
            ->with(compact('tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\System\Requests\LanguageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LanguageRequest $request)
    {
        $this->languageRepository->create($request->all());

        return redirect()->route('admin.language.index')
            ->with('successNotification', __('lixir::system.notification.store', ['attribute' => 'Language']));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\Language $language
     * @return \Illuminate\View\View
     */
    public function edit(Language $language)
    {
        $tabs = Tab::get('system.language');

        return view('lixir::system.language.edit')
            ->with(compact('language', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\System\Requests\LanguageRequest $request
     * @param \Lixir\Database\Models\Language  $language
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LanguageRequest $request, Language $language)
    {
        $language->update($request->all());

        return redirect()->route('admin.language.index')
            ->with('successNotification', __('lixir::system.notification.updated', ['attribute' => 'Language']));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\Language  $language
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Language $language)
    {
        $language->delete();

        return response()->json([
            'success' => true,
            'message' => __('lixir::system.notification.delete', ['attribute' => 'Language']),
        ]);
    }
}
