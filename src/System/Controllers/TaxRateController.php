<?php

namespace Lixir\System\Controllers;

use Lixir\Database\Models\TaxRate;
use Lixir\System\Requests\TaxRateRequest;
use Lixir\Database\Contracts\CountryModelInterface;
use Lixir\Database\Contracts\TaxRateModelInterface;

class TaxRateController
{
    /**
     * TaxRate Repository for Controller.
     * @var \Lixir\Database\Repository\TaxRateRepository
     */
    protected $taxRateRepository;

    /**
     * Country Repository for the State Controller.
     * @var \Lixir\Database\Repository\CountryRepository
     */
    protected $countryRepository;

    /**
     * Construct for the lixir tax rate controller.
     * @param \Lixir\Database\Contracts\TaxRateModelInterface $taxRateRepository
     * @param \Lixir\Database\Contracts\CountryModelInterface $countryRepository
     */
    public function __construct(
        TaxRateModelInterface $taxRateRepository,
        CountryModelInterface $countryRepository
    ) {
        $this->taxRateRepository = $taxRateRepository;
        $this->countryRepository = $countryRepository;
    }

    /**
     * Show Dashboard of an lixir Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $taxRates = $this->taxRateRepository->all();

        return view('lixir::system.tax-rate.index')
            ->with(compact('taxRates'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $typeOptions = TaxRate::RATE_TYPE_OPTIONS;
        $countryOptions = $this->countryRepository->options();

        return view('lixir::system.tax-rate.create')
            ->with(compact('typeOptions', 'countryOptions'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\Cms\Requests\TaxRateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TaxRateRequest $request)
    {
        $this->taxRateRepository->create($request->all());

        return redirect()->route('admin.tax-rate.index')
            ->with('successNotification', __(
                'lixir::system.notification.store',
                ['attribute' => __('lixir::system.tax-rate.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\TaxRate $taxRate
     * @return \Illuminate\View\View
     */
    public function edit(TaxRate $taxRate)
    {
        $typeOptions = TaxRate::RATE_TYPE_OPTIONS;
        $countryOptions = $this->countryRepository->options();

        return view('lixir::system.tax-rate.edit')
            ->with(compact('taxRate', 'typeOptions', 'countryOptions'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\Cms\Requests\TaxRateRequest $request
     * @param \Lixir\Database\Models\TaxRate  $taxRate
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TaxRateRequest $request, TaxRate $taxRate)
    {
        $taxRate->update($request->all());

        return redirect()->route('admin.tax-rate.index')
            ->with('successNotification', __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::system.tax-rate.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\TaxRate  $taxRate
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(TaxRate $taxRate)
    {
        $taxRate->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'lixir::system.notification.delete',
                ['attribute' => __('lixir::system.tax-rate.title')]
            ),
        ]);
    }
}
