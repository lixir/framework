<?php

namespace Lixir\System\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\Currency;
use Lixir\System\Requests\CurrencyRequest;
use Lixir\Database\Contracts\CountryModelInterface;
use Lixir\Database\Contracts\CurrencyModelInterface;

class CurrencyController extends Controller
{
    /**
     * Currency Repository.
     * @var \Lixir\Database\Repository\CurrencyRepository
     */
    protected $currencyRepository;

    /**
     * Currency Repository.
     * @var \Lixir\Database\Repository\CountryRepository
     */
    protected $countryRepository;

    /**
     * Construct for the lixir currency controller.
     * @param \Lixir\Database\Contracts\CurrencyModelInterface $currencyRepository
     * @param \Lixir\Database\Contracts\CountryModelInterface $countryRepository
     */
    public function __construct(
        CurrencyModelInterface $currencyRepository,
        CountryModelInterface $countryRepository
    ) {
        $this->currencyRepository = $currencyRepository;
        $this->countryRepository = $countryRepository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $currencies = $this->currencyRepository->all();

        return view('lixir::system.currency.index')
            ->with(compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('system.currency');
        $currencyCodeOptions = $this->countryRepository->currencyCodeOptions();
        $currencySymbolOptions = $this->countryRepository->currencySymbolOptions();

        return view('lixir::system.currency.create')
            ->with(compact('currencySymbolOptions', 'currencyCodeOptions', 'tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\System\Requests\CurrencyRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CurrencyRequest $request)
    {
        $this->currencyRepository->create($request->all());

        return redirect()->route('admin.currency.index')
            ->with('successNotification', __(
                'lixir::system.notification.store',
                ['attribute' => __('lixir::system.currency.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\Currency $currency
     * @return \Illuminate\View\View
     */
    public function edit(Currency $currency)
    {
        $tabs = Tab::get('system.currency');
        $currencyCodeOptions = $this->countryRepository->currencyCodeOptions();
        $currencySymbolOptions = $this->countryRepository->currencySymbolOptions();

        return view('lixir::system.currency.edit')
            ->with(compact('currency', 'currencySymbolOptions', 'currencyCodeOptions', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\System\Requests\CurrencyRequest $request
     * @param \Lixir\Database\Models\Currency  $currency
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CurrencyRequest $request, Currency $currency)
    {
        $currency->update($request->all());

        return redirect()->route('admin.currency.index')
            ->with('successNotification', __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::system.currency.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\Currency  $currency
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Currency $currency)
    {
        $currency->delete();

        return response()->json([
            'success' => true,
            'message' => __('lixir::system.notification.delete', ['attribute' => __('lixir::system.currency.title')]),
        ]);
    }
}
