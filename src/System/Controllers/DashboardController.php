<?php

namespace Lixir\System\Controllers;

use Lixir\Support\Facades\Widget;

class DashboardController
{
    /**
     * Show Dashboard of an lixir Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $orderWidget = Widget::get('lixir-total-order');
        $customerWidget = Widget::get('lixir-total-customer');
        $revenueWidget = Widget::get('lixir-total-revenue');

        return view('lixir::admin', compact('orderWidget', 'customerWidget', 'revenueWidget'));
    }
}
