<?php

namespace Lixir\System\Controllers;

use Lixir\Database\Models\TaxGroup;
use Lixir\System\Requests\TaxGroupRequest;
use Lixir\Database\Contracts\TaxGroupModelInterface;

class TaxGroupController
{
    /**
     * TaxGroup Repository for Controller.
     * @var \Lixir\Database\Repository\TaxGroupRepository
     */
    protected $taxGroupRepository;

    /**
     * Construct for the lixir tax group controller.
     * @param \Lixir\Database\Contracts\TaxGroupModelInterface $taxGroupRepository
     */
    public function __construct(
        TaxGroupModelInterface $taxGroupRepository
    ) {
        $this->taxGroupRepository = $taxGroupRepository;
    }

    /**
     * Show Dashboard of an lixir Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $taxGroups = $this->taxGroupRepository->all();

        return view('lixir::system.tax-group.index')
            ->with(compact('taxGroups'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('lixir::system.tax-group.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\Cms\Requests\TaxGroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TaxGroupRequest $request)
    {
        $this->taxGroupRepository->create($request->all());

        return redirect()->route('admin.tax-group.index')
            ->with('successNotification', __(
                'lixir::system.notification.store',
                ['attribute' => __('lixir::system.tax-group.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\TaxGroup $taxGroup
     * @return \Illuminate\View\View
     */
    public function edit(TaxGroup $taxGroup)
    {
        return view('lixir::system.tax-group.edit')
            ->with(compact('taxGroup'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\Cms\Requests\TaxGroupRequest $request
     * @param \Lixir\Database\Models\TaxGroup  $taxGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TaxGroupRequest $request, TaxGroup $taxGroup)
    {
        $taxGroup->update($request->all());

        return redirect()->route('admin.tax-group.index')
            ->with('successNotification', __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::system.tax-group.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\TaxGroup  $taxGroup
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(TaxGroup $taxGroup)
    {
        $taxGroup->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'lixir::system.notification.delete',
                ['attribute' => __('lixir::system.tax-group.title')]
            ),
        ]);
    }
}
