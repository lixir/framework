<?php

namespace Lixir\System\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\State;
use Lixir\System\Requests\StateRequest;
use Lixir\Database\Contracts\StateModelInterface;
use Lixir\Database\Contracts\CountryModelInterface;

class StateController extends Controller
{
    /**
     * State Repository for the State Controller.
     * @var \Lixir\Database\Repository\StateRepository
     */
    protected $stateRepository;

    /**
     * Country Repository for the State Controller.
     * @var \Lixir\Database\Repository\CountryRepository
     */
    protected $countryRepository;

    /**
     * Construct for the lixir state controller.
     * @param \Lixir\Database\Contracts\StateModelInterface $stateRepository
     * @param \Lixir\Database\Contracts\CountryModelInterface $countryRepository
     */
    public function __construct(
        StateModelInterface $stateRepository,
        CountryModelInterface $countryRepository
    ) {
        $this->stateRepository = $stateRepository;
        $this->countryRepository = $countryRepository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $states = $this->stateRepository->all();

        return view('lixir::system.state.index')
            ->with(compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('system.state');
        $countryOptions = $this->countryRepository->options();

        return view('lixir::system.state.create')
            ->with(compact('countryOptions', 'tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\System\Requests\StateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StateRequest $request)
    {
        $this->stateRepository->create($request->all());

        return redirect()->route('admin.state.index')
            ->with('successNotification', __(
                'lixir::system.notification.store',
                ['attribute' => __('lixir::system.state.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\State $state
     * @return \Illuminate\View\View
     */
    public function edit(State $state)
    {
        $tabs = Tab::get('system.state');
        $countryOptions = $this->countryRepository->options();

        return view('lixir::system.state.edit')
            ->with(compact('state', 'countryOptions', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\System\Requests\StateRequest $request
     * @param \Lixir\Database\Models\State  $state
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StateRequest $request, State $state)
    {
        $state->update($request->all());

        return redirect()->route('admin.state.index')
            ->with('successNotification', __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir:system.state.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\State  $state
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(State $state)
    {
        $state->delete();

        return response()->json([
            'success' => true,
            'message' => __('lixir::system.notification.delete', ['attribute' => __('lixir:system.state.title')]),
        ]);
    }
}
