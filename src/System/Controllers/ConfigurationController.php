<?php

namespace Lixir\System\Controllers;

use Illuminate\Http\Request;
use Lixir\Support\Facades\Tab;
use Lixir\Database\Contracts\ConfigurationModelInterface;

class ConfigurationController
{
    /**
     * Configuration Repository for the Install Command.
     * @var \Lixir\Database\Repository\ConfigurationRepository
     */
    protected $configurationRepository;

    /**
     * Construct for the lixir configuration controller.
     * @param \Lixir\Database\Contracts\ConfigurationModelInterface $configurationRepository
     */
    public function __construct(
        ConfigurationModelInterface $configurationRepository
    ) {
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * Show Configuration  of an lixir Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $tabs = Tab::get('system.configuration');

        return view('lixir::system.configuration.index')
            ->with('tabs', $tabs)
            ->with('repository', $this->configurationRepository);
    }

    /**
     * Show Configuration  of an lixir Admin.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        foreach ($request->except('_token') as $code => $value) {
            $model = $this->configurationRepository->getModelByCode($code);
            if ($model === null) {
                $this->configurationRepository->create(['code' => $code, 'value' => $value]);
            } else {
                $model->update(['value' => $value]);
            }
        }

        return redirect()->route('admin.configuration.index')
            ->with(
                'successNotification',
                __('lixir::system.notification.save', ['attribute' => __('lixir::system.configuration.title')])
            );
    }
}
