<?php

namespace Lixir;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Lixir\Support\Console\AdminMakeCommand;
use Lixir\Support\Console\InstallCommand;
use Lixir\Support\Middleware\AdminAuth;
use Lixir\Support\Middleware\LixirCore;
use Lixir\Support\Middleware\Permission;
use Lixir\Support\Middleware\RedirectIfAdminAuth;
use Lixir\System\ViewComposers\LayoutComposer;

class LixirServiceProvider extends ServiceProvider
{
    protected $providers = [
        \Lixir\Support\Providers\BreadcrumbProvider::class,
        \Lixir\Support\Providers\CartProvider::class,
        \Lixir\Support\Providers\EventServiceProvider::class,
        \Lixir\Support\Providers\MenuProvider::class,
        \Lixir\Support\Providers\ModelProvider::class,
        \Lixir\Support\Providers\ModuleProvider::class,
        \Lixir\Support\Providers\PaymentProvider::class,
        \Lixir\Support\Providers\PermissionProvider::class,
        \Lixir\Support\Providers\ShippingProvider::class,
        \Lixir\Support\Providers\TabProvider::class,
        \Lixir\Support\Providers\WidgetProvider::class,
    ];

    public function register()
    {
        $this->registerProviders();
        $this->registerConfigData();
        $this->registerRoutePath();
        $this->registerMiddleware();
        $this->registerViewComposerData();
        $this->registerConsoleCommands();
        $this->registerMigrationPath();
        $this->registerViewPath();
    }

    public function boot()
    {
        $this->registerTranslationPath();
        $this->setupPublishFiles();
    }

    public function registerRoutePath()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
    }

    public function registerConsoleCommands()
    {
        $this->commands([InstallCommand::class]);
        $this->commands([AdminMakeCommand::class]);
    }

    public function registerMigrationPath()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    public function registerViewPath()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'lixir');
    }

    public function registerTranslationPath()
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'lixir');
    }

    protected function registerProviders()
    {
        foreach ($this->providers as $provider) {
            App::register($provider);
        }
    }

    protected function registerMiddleware()
    {
        $router = $this->app['router'];
        $router->aliasMiddleware('admin.auth', AdminAuth::class);
        $router->aliasMiddleware('permission', Permission::class);
        $router->aliasMiddleware('admin.guest', RedirectIfAdminAuth::class);
        $router->aliasMiddleware('lixir', LixirCore::class);
    }

    public function registerConfigData()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/lixir.php', 'lixir');

        $lixirConfigData = include __DIR__.'/../config/lixir.php';
        $fileSystemConfig = $this->app['config']->get('filesystems', []);
        $authConfig = $this->app['config']->get('auth', []);
        $this->app['config']->set(
            'filesystems',
            array_merge_recursive(
                $lixirConfigData['filesystems'],
                $fileSystemConfig
            )
        );

        $this->app['config']->set('auth', array_merge_recursive($lixirConfigData['auth'], $authConfig));
    }

    public function registerViewComposerData()
    {
        View::composer('lixir::layouts.app', LayoutComposer::class);
    }

    public function setupPublishFiles()
    {
        $this->publishes([__DIR__.'/../config/lixir.php' => config_path('lixir.php')], 'lixir-config');
        $this->publishes([__DIR__.'/../assets/lixir-admin' => public_path('lixir-admin')], 'lixir-admin');
    }
}
