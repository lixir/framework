<?php

namespace Lixir\Database\Repository;

use Illuminate\Database\Eloquent\Collection;
use Lixir\Database\Models\UserGroup;
use Lixir\Database\Contracts\UserGroupModelInterface;

class UserGroupRepository implements UserGroupModelInterface
{
    /**
     * Create UserGroup Resource into a database.
     * @param array $data
     * @return \Lixir\Database\Models\UserGroup $userGroups
     */
    public function create(array $data): UserGroup
    {
        return UserGroup::create($data);
    }

    /**
     * get all user groups for.
     * @return \Illuminate\Database\Eloquent\Collection $userGroups
     */
    public function all() : Collection
    {
        return UserGroup::all();
    }

    /**
     * get default user group instance.
     * @return \Lixir\Database\Models\UserGroup $userGroup
     */
    public function getIsDefault() : UserGroup
    {
        return UserGroup::whereIsDefault(true)->first();
    }
}
