<?php

namespace Lixir\Database\Repository;

use Illuminate\Database\Eloquent\Collection;
use Lixir\Database\Models\AdminUser;
use Lixir\Database\Contracts\AdminUserModelInterface;

class AdminUserRepository implements AdminUserModelInterface
{
    /**
     * Create AdminUser Resource into a database.
     * @param array $data
     * @return \Lixir\Database\Models\AdminUser $adminUser
     */
    public function create(array $data): AdminUser
    {
        return AdminUser::create($data);
    }

    /**
     * Find AdminUser by given Email in database.
     * @param string $email
     * @return \Lixir\Database\Models\AdminUser $adminUser
     */
    public function findByEmail(string $email) : AdminUser
    {
        return AdminUser::whereEmail($email)->first();
    }

    /**
     * Get all the admin users from the connected database.
     * @return \Illuminate\Database\Eloquent\Collection $adminUsers
     */
    public function all() : Collection
    {
        return AdminUser::all();
    }
}
