<?php

namespace Lixir\Database\Repository;

use Illuminate\Database\Eloquent\Collection;
use Lixir\Database\Models\Attribute;
use Lixir\Database\Contracts\AttributeModelInterface;

class AttributeRepository implements AttributeModelInterface
{
    /**
     * Create Attribute Resource into a database.
     * @param array $data
     * @return \Lixir\Database\Models\Attribute $attribute
     */
    public function create(array $data): Attribute
    {
        return Attribute::create($data);
    }

    /**
     * Find Attribute Resource into a database.
     * @param int $id
     * @return \Lixir\Database\Models\Attribute $attribute
     */
    public function find(int $id): Attribute
    {
        return Attribute::find($id);
    }

    /**
     * Delete Attribute Resource from a database.
     * @param int $id
     * @return int
     */
    public function delete(int $id): int
    {
        return Attribute::destroy($id);
    }

    /**
     * Get all the attributes from the connected database.
     * @return \Illuminate\Database\Eloquent\Collection $attributes
     */
    public function all() : Collection
    {
        return Attribute::all();
    }
}
