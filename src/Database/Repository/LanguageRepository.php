<?php

namespace Lixir\Database\Repository;

use Illuminate\Database\Eloquent\Collection;
use Lixir\Database\Models\Language;
use Lixir\Database\Contracts\LanguageModelInterface;

class LanguageRepository implements LanguageModelInterface
{
    /**
     * Create Language Resource into a database.
     * @param array $data
     * @return \Lixir\Database\Models\Language $language
     */
    public function create(array $data): Language
    {
        return Language::create($data);
    }

    /**
     * get all languages available for this store.
     * @return \Illuminate\Database\Eloquent\Collection $languages
     */
    public function all() : Collection
    {
        return Language::all();
    }
}
