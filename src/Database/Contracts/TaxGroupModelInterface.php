<?php

namespace Lixir\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Lixir\Database\Models\TaxGroup;

interface TaxGroupModelInterface
{
    /**
     * Create TaxGroup Resource into a database.
     * @param array $data
     * @return \Lixir\Database\Models\TaxGroup $taxGroup
     */
    public function create(array $data) : TaxGroup;

    /**
     * find roles for the users.
     * @return \Illuminate\Database\Eloquent\Collection $taxGroups
     */
    public function all() : Collection;
}
