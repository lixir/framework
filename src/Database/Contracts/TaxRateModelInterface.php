<?php

namespace Lixir\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Lixir\Database\Models\TaxRate;

interface TaxRateModelInterface
{
    /**
     * Create TaxRate Resource into a database.
     * @param array $data
     * @return \Lixir\Database\Models\TaxRate $taxGroup
     */
    public function create(array $data) : TaxRate;

    /**
     * find roles for the users.
     * @return \Illuminate\Database\Eloquent\Collection $taxGroups
     */
    public function all() : Collection;
}
