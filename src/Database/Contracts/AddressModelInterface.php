<?php

namespace Lixir\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Lixir\Database\Models\Address;

interface AddressModelInterface
{
    /**
     * Create Address Resource into a database.
     * @param array $data
     * @return \Lixir\Database\Models\Address $address
     */
    public function create(array $data) : Address;

    /**
     * Find Address Resource into a database.
     * @param int $id
     * @return \Lixir\Database\Models\Address $address
     */
    public function find(int $id) : Address;

    /**
     * Get All Addresses from Database via User Id.
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection $addresses
     */
    public function getByUserId(int $userId) : Collection;

    /**
     * Delete Address Resource from a database.
     * @param int $id
     * @return int
     */
    public function delete(int $id) : int;

    /**
     * Get All Address from the database.
     * @return \Illuminate\Database\Eloquent\Collection $addresses
     */
    public function all() : Collection;
}
