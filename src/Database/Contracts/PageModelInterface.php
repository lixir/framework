<?php

namespace Lixir\Database\Contracts;

use Lixir\Database\Models\Page;
use Illuminate\Database\Eloquent\Collection;

interface PageModelInterface
{
    /**
     * Create Page Resource into a database.
     * @param array $data
     * @return \Lixir\Database\Models\Page $page
     */
    public function create(array $data) : Page;

    /**
     * Find Page Resource into a database.
     * @param int $id
     * @return \Lixir\Database\Models\Page $page
     */
    public function find(int $id) : Page;

    /**
     * Find Page Resource into a database.
     * @param string $slug
     * @return \Lixir\Database\Models\Page $page
     */
    public function findBySlug(string $slug);

    /**
     * Delete Page Resource from a database.
     * @param int $id
     * @return int
     */
    public function delete(int $id) : int;

    /**
     * Get All Page from the database.
     * @return \Illuminate\Database\Eloquent\Collection $pages
     */
    public function all() : Collection;
}
