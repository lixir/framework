<?php

namespace Lixir\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Lixir\Database\Models\Language;

interface LanguageModelInterface
{
    /**
     * Create Language Resource into a database.
     * @param array $data
     * @return \Lixir\Database\Models\Language $language
     */
    public function create(array $data) : Language;

    /**
     * find roles for the users.
     * @return \Illuminate\Database\Eloquent\Collection $languages
     */
    public function all() : Collection;
}
