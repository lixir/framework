<?php

namespace Lixir\Promotion\ViewModels\Promotion;

use Lixir\Database\Contracts\PromotionCodeModelInterface;
use Lixir\Database\Models\PromotionCode;
use Lixir\Support\Facades\Tab;
use Spatie\ViewModels\ViewModel;

class EditViewModel extends ViewModel
{
    protected $model;

    public function __construct(PromotionCode $promotionCode)
    {
        $this->model = $promotionCode;
    }

    public function promotionCode()
    {
        return $this->model;
    }

    public function tabs()
    {
        return Tab::get('promotion.promotion-code');
    }

    public function typeOptions()
    {
        return PromotionCode::TYPEOPTIONS;
    }
}
