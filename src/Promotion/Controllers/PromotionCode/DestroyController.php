<?php
namespace Lixir\Promotion\Controllers\PromotionCode;

use Lixir\Database\Models\PromotionCode;
use Lixir\Promotion\ViewModels\PromotionTableViewModel;

class DestroyController
{

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\PromotionCode  $promotionCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(PromotionCode $promotionCode)
    {
        $promotionCode->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'lixir::system.notification.delete',
                ['attribute' => __('lixir::promotion.promotion-code.title')]
            ),
        ]);
    }
}
