<?php
namespace Lixir\Promotion\Controllers\PromotionCode;

use Lixir\Promotion\ViewModels\PromotionTableViewModel;

class TableController
{
    /**
     * Show available promotion list of an lixir Admin.
     * @return \Illuminate\View\View
     */
    public function __invoke()
    {
        return view('lixir::promotion.promotion-code.index', new PromotionTableViewModel);
    }
}
