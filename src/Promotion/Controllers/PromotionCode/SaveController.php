<?php
namespace Lixir\Promotion\Controllers\PromotionCode;

use Lixir\Database\Models\PromotionCode;
use Lixir\Promotion\Requests\PromotionCodeRequest;

class SaveController
{
    /**
     * Save the specified resource from storage.
     * @param \Lixir\Promotion\Requests\PromotionCodeRequest  $request
     * @param \Lixir\Database\Models\PromotionCode $promotionCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(PromotionCodeRequest $request, PromotionCode $promotionCode)
    {
        $promotionCode->fill($request->all())->save();

        return redirect()->route('admin.promotion.code.table');
    }
}
