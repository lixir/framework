<?php
namespace Lixir\Promotion\Controllers\PromotionCode;

use Lixir\Database\Models\PromotionCode;
use Lixir\Promotion\ViewModels\Promotion\EditViewModel;

class EditController
{
    /**
     * Create/Edit the specified resource from storage.
     * @param \Lixir\Database\Models\PromotionCode  $promotionCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(PromotionCode $promotionCode)
    {
        return view(
            'lixir::promotion.promotion-code.edit',
            new EditViewModel($promotionCode)
        );
    }
}
