<?php

namespace Lixir\Modules\Console;

use Illuminate\Support\ServiceProvider;

class Provider extends ServiceProvider
{
    /**
     * Command Name for the lixir Console.
     *
     * @var array
     */
    protected $commandName = [
        'lixir.module.install',
        'lixir.module.make',
        'lixir.controller.make',
    ];

    /**
     * Command Identifier for the lixir Console.
     *
     * @var array
     */
    protected $commands;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands();
    }

    protected function registerCommands()
    {
        foreach ($this->commandName as $commandName) {
            $methodName = 'register'.implode(array_map('ucfirst', explode('.', $commandName)));
            $this->$methodName();

            $this->commands[] = 'command.'.$commandName;
        }

        $this->commands($this->commands);
    }

    /**
     * Register the lixir Module Make .
     *
     * @return void
     */
    protected function registerlixirModuleMake()
    {
        $this->app->singleton('command.lixir.module.make', function ($app) {
            return new ModuleMakeCommand($app['files']);
        });
    }

    /**
     * Register the lixir Module Install .
     *
     * @return void
     */
    protected function registerlixirModuleInstall()
    {
        $this->app->singleton('command.lixir.module.install', function ($app) {
            return new ModuleInstallCommand($app['migrator']);
        });
    }

    /**
     * Register lixir Module Controller Make Command.
     *
     * @return void
     */
    protected function registerlixirControllerMake()
    {
        $this->app->singleton('command.lixir.controller.make', function ($app) {
            return new ControllerMakeCommand($app['files']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return $this->commands;
    }
}
