<?php

namespace Lixir\Cms\Controllers;

use Lixir\Cms\Requests\MenuRequest;
use Lixir\Database\Models\MenuGroup;
use Lixir\Support\Facades\Menu;
use Lixir\Database\Contracts\MenuModelInterface;
use Lixir\Database\Contracts\CategoryModelInterface;
use Lixir\Database\Contracts\MenuGroupModelInterface;

class MenuGroupController
{
    /**
     * Menu Repository for the Menu Controller.
     * @var \Lixir\Database\Repository\MenuRepository
     */
    protected $menuRepository;

    /**
     * Menu Group Repository for the Menu Controller.
     * @var \Lixir\Database\Repository\MenuGroupRepository
     */
    protected $menuGroupRepository;

    /**
     * Menu Controller for the Install Command.
     * @var \Lixir\Database\Repository\CategoryRepository
     */
    protected $categoryRepository;

    /**
     * Construct for the lixir install command.
     * @param \Lixir\Database\Contracts\MenuModelInterface $menuRepository
     * @param \Lixir\Database\Contracts\MenuGroupModelInterface $menuGroupRepository
     * @param \Lixir\Database\Contracts\CategoryModelInterface $categoryRepository
     */
    public function __construct(MenuModelInterface $menuRepository, MenuGroupModelInterface $menuGroupRepository, CategoryModelInterface $categoryRepository)
    {
        $this->menuRepository = $menuRepository;
        $this->menuGroupRepository = $menuGroupRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $menuGroups = $this->menuGroupRepository->all();

        return view('lixir::cms.menu.index')->with(compact('menuGroups'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->categoryRepository->getCategoryOptionForMenuBuilder();
        $frontMenus = Menu::frontMenus();
        $menus = [];


        return view('lixir::cms.menu.create')->with(compact('frontMenus', 'menus', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\Cms\Requests\MenuRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(MenuRequest $request)
    {
        $menuGroup = $this->menuGroupRepository->create($request->all());
        $menus = json_decode($request->get('menu_json'));

        $this->saveMenus($menuGroup, $menus);

        return redirect()
            ->route('admin.menu-group.index')
            ->with('successNotification', __('lixir::system.notification.store', ['attribute' => 'Menu']));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\MenuGroup $menuGroup
     * @return \Illuminate\View\View
     */
    public function edit(MenuGroup $menuGroup)
    {
        $categories = $this->categoryRepository->getCategoryOptionForMenuBuilder();
        $frontMenus = Menu::frontMenus();
        $menus = $this->menuGroupRepository->getTreeByIdentifier($menuGroup->identifier);

        return view('lixir::cms.menu.edit')->with(compact('categories', 'menus', 'frontMenus', 'menuGroup'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\Cms\Requests\MenuRequest $request
     * @param \Lixir\Database\Models\MenuGroup  $menuGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(MenuRequest $request, MenuGroup $menuGroup)
    {
        $menuGroup->menus()->delete();
        $menuGroup->update($request->all());
        $menus = json_decode($request->get('menu_json'));

        $this->saveMenus($menuGroup, $menus);

        return redirect()->route('admin.menu-group.index')->with('successNotification', __('lixir::system.notification.updated', ['attribute' => 'Menu']));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\MenuGroup  $menuGroup
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(MenuGroup $menuGroup)
    {
        $menuGroup->delete();

        return response()->json([
            'success' => true,
            'message' => __('lixir::system.notification.delete', ['attribute' => 'Menu']),
        ]);
    }

    /**
     * set the categories url for menu.
     * @param \Lixir\Database\Models\MenuGroup
     * @param \Lixir\Cms\Requests\MenuRequest $request
     * @param \\Lixir\Database\Models\Menu $parent
     * @return void
     */
    public function saveMenus(MenuGroup $menuGroup, $menus, $parent = null)
    {
        foreach ($menus as $menu) {
            $data = [
                'name' => $menu->name,
                'url' => $menu->url,
            ];

            if ($parent !== null) {
                $data['parent_id'] = $parent->id;
            }
            $menuModel = $menuGroup->menus()->create($data);

            if (isset($menu->submenus) && count($menu->submenus) > 0) {
                $this->saveMenus($menuGroup, $menu->submenus, $menuModel);
            }
        }
    }
}
