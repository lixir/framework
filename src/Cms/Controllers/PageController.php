<?php

namespace Lixir\Cms\Controllers;

use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\Page;
use Lixir\Cms\Requests\PageRequest;
use Lixir\Database\Contracts\PageModelInterface;
use Lixir\Support\Facades\Widget;

class PageController
{
    /**
     * Page Repository for the Install Command.
     * @var \Lixir\Database\Repository\PageRepository
     */
    protected $pageRepository;

    /**
     * Construct for the lixir install command.
     * @param \Lixir\Database\Contracts\PageModelInterface $pageRepository
     */
    public function __construct(
        PageModelInterface $pageRepository
    ) {
        $this->pageRepository = $pageRepository;
    }

    /**
     * Show Dashboard of an lixir Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $pages = $this->pageRepository->all();

        return view('lixir::cms.page.index')
            ->with(compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $widgets = Widget::options();
        $tabs = Tab::get('cms.page');

        return view('lixir::cms.page.create')
            ->with(compact('tabs'))
            ->with('widgets', $widgets);
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\Cms\Requests\PageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PageRequest $request)
    {
        $this->pageRepository->create($request->all());

        return redirect()->route('admin.page.index')
            ->with('successNotification', __('lixir::system.notification.store', ['attribute' => 'Page']));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\Page $page
     * @return \Illuminate\View\View
     */
    public function edit(Page $page)
    {
        $tabs = Tab::get('cms.page');
        $widgets = Widget::options();

        return view('lixir::cms.page.edit')
            ->with(compact('page', 'tabs'))
            ->with(compact('widgets'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\Cms\Requests\PageRequest $request
     * @param \Lixir\Database\Models\Page  $page
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PageRequest $request, Page $page)
    {
        $page->update($request->all());

        return redirect()->route('admin.page.index')
            ->with('successNotification', __('lixir::system.notification.updated', ['attribute' => 'Page']));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\Page  $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Page $page)
    {
        $page->delete();

        return response()->json([
            'success' => true,
            'message' => __('lixir::system.notification.delete', ['attribute' => 'Page']),
        ]);
    }
}
