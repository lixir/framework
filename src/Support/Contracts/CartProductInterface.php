<?php

namespace Lixir\Support\Contracts;

interface CartProductInterface
{
    public function name();

    public function attributes(array $attributes);

    public function image();

    public function slug();

    public function qty();

    public function total();
}
