<?php

namespace Lixir\Support\Contracts;

interface WidgetInterface
{
    public function label();

    public function type();
}
