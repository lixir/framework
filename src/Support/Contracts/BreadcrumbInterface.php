<?php

namespace Lixir\Support\Contracts;

interface BreadcrumbInterface
{
    public function label();

    public function route();
}
