<?php

namespace Lixir\Support\Console;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Lixir\Database\Contracts\CurrencyModelInterface;
use Lixir\Database\Contracts\LanguageModelInterface;
use Lixir\Database\Contracts\OrderStatusModelInterface;
use Lixir\Database\Contracts\RoleModelInterface;
use Lixir\Database\Contracts\UserGroupModelInterface;
use Lixir\Database\Models\Role;

class InstallCommand extends Command
{
    protected $roleRepository;

    protected $orderStatusRepository;

    protected $userGroupRepository;

    protected $currencyRepository;

    protected $languageRepository;

    public function __construct(RoleModelInterface $roleRepository, CurrencyModelInterface $currencyRepository, LanguageModelInterface $languageRepository, UserGroupModelInterface $userGroupRepository, OrderStatusModelInterface $orderStatusRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->currencyRepository = $currencyRepository;
        $this->languageRepository = $languageRepository;
        $this->userGroupRepository = $userGroupRepository;
        $this->orderStatusRepository = $orderStatusRepository;

        parent::__construct();
    }

    protected $name = 'lixir:install';

    protected $description = 'Install lixir E-commerce';

    public function handle()
    {
        $this->call('migrate:fresh');
        $this->call('storage:link');

        if ($this->confirm('Would you like to install Dummy Data?')) {
            $this->call('lixir:module:install', ['indentifier' => 'lixir-demodata']);
        }

        $roleData = ['name' => Role::ADMIN];

        $this->roleRepository->create($roleData);

        $this->createCurrency();
        $this->createLanguage();
        $this->createDefaultUserGroup();
        $this->createOrderStatus();
        $this->alterUserTable();

        $this->call('lixir:admin:make');
        $this->info('lixir Installed Successfully!');
    }

    public function createCurrency()
    {
        $data = [
            'name' => 'US Dollar',
            'code' => 'usd',
            'symbol' => '$',
            'conversion_rate' => 1,
            'status' => 'ENABLED'
        ];

        $this->currencyRepository->create($data);
    }

    public function createDefaultUserGroup()
    {
        $data = [
            'name' => 'Default Group',
            'is_default' => 1
        ];

        $this->userGroupRepository->create($data);
    }

    public function createOrderStatus()
    {
        $defaultStatus = $this->orderStatusRepository->create(['name' => 'Pending']);
        $defaultStatus->is_default = 1;
        $defaultStatus->save();

        $this->orderStatusRepository->create(['name' => 'Processing']);
        $this->orderStatusRepository->create(['name' => 'Completed']);
    }

    public function createLanguage()
    {
        $data = [
            'name' => 'English',
            'code' => 'en',
            'is_default' => 1
        ];

        $this->languageRepository->create($data);
    }

    public function alterUserTable()
    {
        $user = config('lixir.model.user');

        try {
            $model = resolve($user);
        } catch (\Exception $e) {
            $model = null;
        }

        if ($model !== null) {
            $table = $model->getTable();

            if (Schema::hasTable($table)) {
                Schema::table($table, function (Blueprint $table) {
                    $table->unsignedBigInteger('user_group_id')->nullable()->default(null);
                });
            }
        }
    }
}
