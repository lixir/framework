<?php

namespace Lixir\Support\Providers;

use Lixir\Tab\Manager;
use Lixir\Tab\TabItem;
use Illuminate\Support\ServiceProvider;
use Lixir\Support\Facades\Tab;

class TabProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     * @var bool
     */
    protected $defer = true;

    public function boot()
    {
        $this->registerTabs();
    }

    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {
        $this->registerManager();
        $this->app->singleton('tab', 'Lixir\Tab\Manager');
    }

    /**
     * Register the tab Manager Instance.
     * @return void
     */
    protected function registerManager()
    {
        $this->app->singleton('tab', function () {
            new Manager();
        });
    }

    /**
     * Get the services provided by the provider.
     * @return array
     */
    public function provides()
    {
        return ['tab', 'Lixir\Tab\Manager'];
    }

    /**
     * Register Tabs for the Different CRUD operations.
     * @return void
     */
    public function registerTabs()
    {

        Tab::put('promotion.promotion-code', function (TabItem $tab) {
            $tab->key('promotion.promotion.code')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::promotion.promotion-code._fields');
        });


        Tab::put('catalog.product', function (TabItem $tab) {
            $tab->key('catalog.product.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::catalog.product._fields');
        });

        Tab::put('catalog.product', function (TabItem $tab) {
            $tab->key('catalog.product.image')
                ->label('lixir::system.tab.images')
                ->view('lixir::catalog.product.cards.images');
        });
        Tab::put('catalog.product', function (TabItem $tab) {
            $tab->key('catalog.product.property')
                ->label('lixir::system.tab.property')
                ->view('lixir::catalog.product.cards.property');
        });
        Tab::put('catalog.product', function (TabItem $tab) {
            $tab->key('catalog.product.attribute')
                ->label('lixir::system.tab.attribute')
                ->view('lixir::catalog.product.cards.attribute');
        });

        /****** CATALOG CATEGORY TABS *******/
        Tab::put('catalog.category', function (TabItem $tab) {
            $tab->key('catalog.category.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::catalog.category._fields');
        });

        /****** CATALOG PROPERTY TABS *******/
        Tab::put('catalog.property', function (TabItem $tab) {
            $tab->key('catalog.property.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::catalog.property._fields');
        });
        /****** CATALOG ATTRIBUTE TABS *******/
        Tab::put('catalog.attribute', function (TabItem $tab) {
            $tab->key('catalog.attribute.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::catalog.attribute._fields');
        });
        /******CMS PAGES TABS  *******/
        Tab::put('cms.page', function (TabItem $tab) {
            $tab->key('cms.page.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::cms.page._fields');
        });
        /******ORDER ORDER STATUS TABS  *******/
        Tab::put('order.order-status', function (TabItem $tab) {
            $tab->key('order.order-status.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::order.order-status._fields');
        });
        /******USER USER GROUP TABS  *******/
        Tab::put('user.user-group', function (TabItem $tab) {
            $tab->key('user.user-group.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::user.user-group._fields');
        });
        /******USER ADMIN USER TABS  *******/
        Tab::put('user.admin-user', function (TabItem $tab) {
            $tab->key('user.admin-user.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::user.admin-user._fields');
        });
        /******SYSTEM CURRENCY TABS  *******/
        Tab::put('system.currency', function (TabItem $tab) {
            $tab->key('system.currency.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::system.currency._fields');
        });
        /******SYSTEM STATE TABS  *******/
        Tab::put('system.state', function (TabItem $tab) {
            $tab->key('system.state.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::system.state._fields');
        });
        /******SYSTEM ROLE TABS  *******/
        Tab::put('system.role', function (TabItem $tab) {
            $tab->key('system.role.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::system.role._fields');
        });
        /******SYSTEM ROLE TABS  *******/
        Tab::put('system.language', function (TabItem $tab) {
            $tab->key('system.language.info')
                ->label('lixir::system.tab.basic_info')
                ->view('lixir::system.language._fields');
        });

        /******SYSTEM CONFIGURATION TABS  *******/
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.basic')
                ->label('lixir::system.tab.basic_configuration')
                ->view('lixir::system.configuration.cards.basic');
        });
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.user')
                ->label('lixir::system.tab.user_configuration')
                ->view('lixir::system.configuration.cards.user');
        });
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.tax')
                ->label('lixir::system.tab.tax_configuration')
                ->view('lixir::system.configuration.cards.tax');
        });
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.shipping')
                ->label('lixir::system.tab.shipping_configuration')
                ->view('lixir::system.configuration.cards.shipping');
        });
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.payment')
                ->label('lixir::system.tab.payment_configuration')
                ->view('lixir::system.configuration.cards.payment');
        });
    }
}
