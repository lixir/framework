<?php

namespace Lixir\Support\Providers;

use Illuminate\Support\ServiceProvider;
use Lixir\Database\Repository\MenuRepository;
use Lixir\Database\Repository\PageRepository;
use Lixir\Database\Repository\RoleRepository;
use Lixir\Database\Repository\OrderRepository;
use Lixir\Database\Repository\StateRepository;
use Lixir\Database\Contracts\MenuModelInterface;
use Lixir\Database\Contracts\PageModelInterface;
use Lixir\Database\Contracts\RoleModelInterface;
use Lixir\Database\Repository\AddressRepository;
use Lixir\Database\Repository\CountryRepository;
use Lixir\Database\Repository\ProductRepository;
use Lixir\Database\Repository\TaxRateRepository;
use Lixir\Database\Contracts\OrderModelInterface;
use Lixir\Database\Contracts\StateModelInterface;
use Lixir\Database\Repository\CategoryRepository;
use Lixir\Database\Repository\CurrencyRepository;
use Lixir\Database\Repository\LanguageRepository;
use Lixir\Database\Repository\PropertyRepository;
use Lixir\Database\Repository\TaxGroupRepository;
use Lixir\Database\Repository\AdminUserRepository;
use Lixir\Database\Repository\AttributeRepository;
use Lixir\Database\Repository\MenuGroupRepository;
use Lixir\Database\Repository\UserGroupRepository;
use Lixir\Database\Contracts\AddressModelInterface;
use Lixir\Database\Contracts\CountryModelInterface;
use Lixir\Database\Contracts\ProductModelInterface;
use Lixir\Database\Contracts\TaxRateModelInterface;
use Lixir\Database\Repository\PermissionRepository;
use Lixir\Database\Contracts\CategoryModelInterface;
use Lixir\Database\Contracts\CurrencyModelInterface;
use Lixir\Database\Contracts\LanguageModelInterface;
use Lixir\Database\Contracts\PropertyModelInterface;
use Lixir\Database\Contracts\TaxGroupModelInterface;
use Lixir\Database\Repository\OrderStatusRepository;
use Lixir\Database\Contracts\AdminUserModelInterface;
use Lixir\Database\Contracts\AttributeModelInterface;
use Lixir\Database\Contracts\MenuGroupModelInterface;
use Lixir\Database\Contracts\UserGroupModelInterface;
use Lixir\Database\Repository\OrderProductRepository;
use Lixir\Database\Repository\ProductImageRepository;
use Lixir\Database\Repository\PromotionCodeRepository;
use Lixir\Database\Contracts\PermissionModelInterface;
use Lixir\Database\Repository\ConfigurationRepository;
use Lixir\Database\Contracts\OrderStatusModelInterface;
use Lixir\Database\Repository\CategoryFilterRepository;
use Lixir\Database\Contracts\OrderProductModelInterface;
use Lixir\Database\Contracts\ProductImageModelInterface;
use Lixir\Database\Contracts\PromotionCodeModelInterface;
use Lixir\Database\Contracts\ConfigurationModelInterface;
use Lixir\Database\Contracts\CategoryFilterModelInterface;
use Lixir\Database\Repository\AttributeProductValueRepository;
use Lixir\Database\Repository\OrderProductAttributeRepository;
use Lixir\Database\Repository\AttributeDropdownOptionRepository;
use Lixir\Database\Contracts\AttributeProductValueModelInterface;
use Lixir\Database\Contracts\OrderProductAttributeModelInterface;
use Lixir\Database\Contracts\AttributeDropdownOptionModelInterface;

class ModelProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Models Array list to bind with It's Contact.
     * @var array
     */
    protected $models = [
        AddressModelInterface::class => AddressRepository::class,
        AdminUserModelInterface::class => AdminUserRepository::class,
        AttributeModelInterface::class => AttributeRepository::class,
        AttributeDropdownOptionModelInterface::class => AttributeDropdownOptionRepository::class,
        AttributeProductValueModelInterface::class => AttributeProductValueRepository::class,
        CategoryModelInterface::class => CategoryRepository::class,
        CategoryFilterModelInterface::class => CategoryFilterRepository::class,
        ConfigurationModelInterface::class => ConfigurationRepository::class,
        CountryModelInterface::class => CountryRepository::class,
        CurrencyModelInterface::class => CurrencyRepository::class,
        LanguageModelInterface::class => LanguageRepository::class,
        OrderModelInterface::class => OrderRepository::class,
        OrderProductModelInterface::class => OrderProductRepository::class,
        OrderProductAttributeModelInterface::class => OrderProductAttributeRepository::class,
        OrderStatusModelInterface::class => OrderStatusRepository::class,
        PermissionModelInterface::class => PermissionRepository::class,
        PageModelInterface::class => PageRepository::class,
        PromotionCodeModelInterface::class => PromotionCodeRepository::class,
        ProductModelInterface::class => ProductRepository::class,
        ProductImageModelInterface::class => ProductImageRepository::class,
        MenuModelInterface::class => MenuRepository::class,
        MenuGroupModelInterface::class => MenuGroupRepository::class,
        PropertyModelInterface::class => PropertyRepository::class,
        RoleModelInterface::class => RoleRepository::class,
        StateModelInterface::class => StateRepository::class,
        UserGroupModelInterface::class => UserGroupRepository::class,
        TaxGroupModelInterface::class => TaxGroupRepository::class,
        TaxRateModelInterface::class => TaxRateRepository::class,
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerModelContracts();
    }

    /**
     * Bind The Eloquent Model with their contract.
     *
     * @return void
     */
    protected function registerModelContracts()
    {
        foreach ($this->models as $interface => $repository) {
            $this->app->bind($interface, $repository);
        }
    }
}
