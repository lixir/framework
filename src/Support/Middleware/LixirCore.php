<?php

namespace Lixir\Support\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Lixir\Database\Contracts\CurrencyModelInterface;

class LixirCore
{
    protected $currencyRepository;

    public function __construct(CurrencyModelInterface $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    public function handle($request, Closure $next)
    {
        $this->setDefaultCurrency();

        return $next($request);
    }

    public function setDefaultCurrency()
    {
        if (!Session::has('default.currency')) {
            $currency = $this->currencyRepository->all()->first();
            Session::put('default.currency', $currency);
        }
    }
}
