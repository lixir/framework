<?php

namespace Lixir\User\Controllers;

use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\UserGroup;
use Lixir\User\Requests\UserGroupRequest;
use Lixir\Database\Contracts\UserGroupModelInterface;

class UserGroupController
{
    /**
     * UserGroup Repository for controller.
     * @var \Lixir\Database\Repository\UserGroupRepository
     */
    protected $userGroupRepository;

    /**
     * Construct for the lixir user group controller.
     * @param \Lixir\Database\Contracts\UserGroupModelInterface $userGroupRepository
     */
    public function __construct(
        UserGroupModelInterface $userGroupRepository
    ) {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * Show Dashboard of an lixir Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $userGroups = $this->userGroupRepository->all();

        return view('lixir::user.user-group.index')
            ->with(compact('userGroups'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('user.user-group');

        return view('lixir::user.user-group.create')
            ->with(compact('tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\Cms\Requests\UserGroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserGroupRequest $request)
    {
        $this->userGroupRepository->create($request->all());

        return redirect()->route('admin.user-group.index')
            ->with('successNotification', __(
                'lixir::system.notification.store',
                ['attribute' => __('lixir::user.user-group.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\UserGroup $userGroup
     * @return \Illuminate\View\View
     */
    public function edit(UserGroup $userGroup)
    {
        $tabs = Tab::get('user.user-group');

        return view('lixir::user.user-group.edit')
            ->with(compact('userGroup', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\Cms\Requests\UserGroupRequest $request
     * @param \Lixir\Database\Models\UserGroup  $userGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserGroupRequest $request, UserGroup $userGroup)
    {
        if ($request->get('is_default')) {
            $group = $this->userGroupRepository->getIsDefault();
            $group->update(['is_default' => 0]);
        }

        $userGroup->update($request->all());

        return redirect()->route('admin.user-group.index')
            ->with('successNotification', __(
                'lixir::system.notification.updated',
                ['attribute' => __('lixir::user.user-group.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\UserGroup  $userGroup
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(UserGroup $userGroup)
    {
        $userGroup->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'lixir::system.notification.delete',
                ['attribute' => __('lixir::user.user-group.title')]
            ),
        ]);
    }
}
