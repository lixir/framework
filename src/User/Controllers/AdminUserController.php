<?php

namespace Lixir\User\Controllers;

use Illuminate\Routing\Controller;
use Lixir\Support\Facades\Tab;
use Lixir\Database\Models\AdminUser;
use Lixir\User\Requests\AdminUserRequest;
use Lixir\User\Requests\AdminUserImageRequest;
use Lixir\Database\Contracts\RoleModelInterface;
use Lixir\Database\Contracts\AdminUserModelInterface;

class AdminUserController extends Controller
{
    /**
     * AdminUser Repository.
     * @var \Lixir\Database\Repository\AdminUserRepository
     */
    protected $adminUserRepository;

    /**
     * Role Repository.
     * @var \Lixir\Database\Repository\RoleRepository
     */
    protected $roleRepository;

    /**
     * Construct for the lixir User Controller.
     * @param \Lixir\Database\Contracts\AdminUserModelInterface $adminUserRepository
     * @param \Lixir\Database\Contracts\RoleModelInterface $roleRepository
     */
    public function __construct(
        AdminUserModelInterface $adminUserRepository,
        RoleModelInterface $roleRepository
    ) {
        $this->adminUserRepository = $adminUserRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $adminUsers = $this->adminUserRepository->all();

        return view('lixir::user.admin-user.index')
            ->with(compact('adminUsers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('user.admin-user');
        $roleOptions = $this->roleRepository->options();

        return view('lixir::user.admin-user.create')
            ->with(compact('roleOptions', 'tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Lixir\System\Requests\AdminUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminUserRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);

        $this->adminUserRepository->create($request->all());

        return redirect()->route('admin.admin-user.index')
            ->with('successNotification', __('lixir::user.notification.store', ['attribute' => 'AdminUser']));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Lixir\Database\Models\AdminUser $adminUser
     * @return \Illuminate\View\View
     */
    public function edit(AdminUser $adminUser)
    {
        $tabs = Tab::get('user.admin-user');
        $roleOptions = $this->roleRepository->options();

        return view('lixir::user.admin-user.edit')
            ->with(compact('adminUser', 'roleOptions', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Lixir\System\Requests\AdminUserRequest $request
     * @param \Lixir\Database\Models\AdminUser  $adminUser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminUserRequest $request, AdminUser $adminUser)
    {
        $adminUser->update($request->all());

        return redirect()->route('admin.admin-user.index')
            ->with('successNotification', __('lixir::user.notification.updated', ['attribute' => 'AdminUser']));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Lixir\Database\Models\AdminUser  $adminUser
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(AdminUser $adminUser)
    {
        $adminUser->delete();

        return response()->json([
            'success' => true,
            'message' => __('lixir::user.notification.delete', ['attribute' => 'AdminUser']),
        ]);
    }

    /**
     * upload user image to file system.
     * @param \Lixir\System\Requests\AdminUserImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(AdminUserImageRequest $request)
    {
        $image = $request->file('image_file');
        $path = $image->store('uploads/users', 'public');

        return response()->json([
            'success' => true,
            'path' => $path,
            'message' => __('lixir::user.notification.upload', ['attribute' => 'Admin User Image']),
        ]);
    }
}
