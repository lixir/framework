<?php

namespace Lixir\User\Observers;

use Lixir\Database\Contracts\ConfigurationModelInterface;
use Lixir\Database\Contracts\UserGroupModelInterface;
use Lixir\Widget\TotalCustomer;

class UserObserver
{
    /**
     * UserGroup Repository for controller.
     * @var \Lixir\Database\Repository\UserGroupRepository
     */
    protected $userGroupRepository;

    /**
     * Construct for the lixir user group controller.
     * @param \Lixir\Database\Repository\UserGroupRepository $userGroupRepository
     */
    public function __construct(
        UserGroupModelInterface $userGroupRepository
    ) {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * Handle the User "created" event.
     *
     * @param mixed $user
     * @return void
     */
    public function created($user)
    {
        $userGroup = $this->userGroupRepository->getIsDefault();
        $user->user_group_id = $userGroup->id;
        $user->save();
    }
}
