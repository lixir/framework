<a-col :span="4" :xs="24" :sm="12" :md="6">
    <a-card title="{{ __('lixir::system.total-customer') }}" class="dashboard-widget mt-1 warning">
        <p class="amount">{{ $value }}</p>
    </a-card>
</a-col>
