@extends('lixir::layouts.app')

@section('meta_title')
    {{ __('lixir::system.state.edit.title') }}: Lixir E-Commerce Admin Dashboard
@endsection

@section('page_title')
    {{ __('lixir::system.state.edit.title') }}
@endsection

@section('content')
<a-row type="flex" justify="center">
    <a-col :span="24">
        <state-save base-url="{{ asset(config('lixir.admin_url')) }}" :state="{{ $state }}" inline-template>
        <div>
            <a-form
                :form="stateForm"
                method="post"
                action="{{ route('admin.state.update', $state->id) }}"
                @submit="handleSubmit"
            >
                @csrf
                @method('put')
                <a-tabs tabbar-gutter="15" tab-position="left" default-active-key="system.state.info">
                @foreach ($tabs as $tab)
                    <a-tab-pane :force-render="true" tab="{{ $tab->label() }}" key="{{ $tab->key() }}">
                        @php
                            $path = $tab->view();
                        @endphp
                        @include($path)
                    </a-tab-pane>
                @endforeach
                </a-tabs>



                <a-form-item>
                    <a-button
                        type="primary"
                        html-type="submit"
                    >
                        {{ __('lixir::system.btn.save') }}
                    </a-button>

                    <a-button
                        class="ml-1"
                        type="default"
                        v-on:click.prevent="cancelState"
                    >
                        {{ __('lixir::system.btn.cancel') }}
                    </a-button>
                </a-form-item>
            </a-form>
            </div>
        </state-save>
    </a-col>
</a-row>
@endsection
