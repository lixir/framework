<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('meta_title', 'Lixir E-Commerce')</title>

    <script defer src="{{ asset('lixir-admin/js/app.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('lixir-admin/css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <password-reset-page inline-template>
            <div>
                <a-row type="flex" align="middle">
                    <a-col :span="12">
                        <a-row type="flex">
                        <a-col :span="20" :offset="2">

                            <a-card title="{{ __('lixir::system.forgot-password-title') }}">
                                <a-form
                                    :form="form"
                                    method="post"
                                    action="{{ route('admin.password.email') }}"
                                    @submit="handleSubmit"
                                >
                                    @csrf()
                                    <a-form-item
                                        @if ($errors->has('email'))
                                            validate-status="error"
                                            help="{{ $errors->first('email') }}"
                                        @endif
                                        label="{{ __('lixir::system.email') }}">
                                    <a-input
                                        :auto-focus="true"
                                        name="email"
                                        v-decorator="[
                                        'email',
                                        {
                                            rules: [
                                                {   required: true,
                                                    message: '{{ __('lixir::validation.required', ['attribute' => 'email']) }}'
                                                }
                                            ]
                                        }
                                        ]"
                                    />
                                    </a-form-item>

                                    <a-form-item>
                                        <a-button
                                            type="primary"
                                            :loading="loadingSubmitBtn"
                                            html-type="submit"
                                        >
                                            {{ __('lixir::system.password-reset-btn') }}
                                        </a-button>

                                    </a-form-item>
                                </a-form>
                            </a-card>
                        </a-col>
                        </a-row>
                    </a-col>

                    <a-col :span="12">
                     <a-row type="flex" align="middle" class="h-100 text-center">
                      <a-col :span="24">
                            <img
                                class="height-100"
                                src="{{ asset('lixir-admin/images/octa_admin_login.svg')}}"
                                width="55%" alt="Lixir Admin Login" />
                        </a-col>
                        </a-row>
                    </a-col>
                </a-row>
            </div>

        </password-reset-page>
    </div>
    @stack('scripts')
</body>
</html>
