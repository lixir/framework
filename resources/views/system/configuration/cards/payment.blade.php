 <a-form-item
    @if ($errors->has('site_name'))
        validate-status="error"
        help="{{ $errors->first('site_name') }}"
    @endif
    label="{{ __('lixir::system.configuration.payment.payment_name') }}">
    <a-input
        :auto-focus="true"
        name="site_name"
        v-decorator="[
        'site_name',
        {initialValue: '{{ ($repository->getValueByCode('site_name')) ?? '' }}'},
        {rules:
            [
                {   required: true,
                    message: '{{ __('lixir::validation.required', ['attribute' => __('lixir::system.configuration.basic.site_name')]) }}'
                }
            ]
        }
        ]"
    ></a-input>
</a-form-item>
