@extends('lixir::layouts.app')

@section('meta_title')
    {{ __('lixir::system.tax-rate.edit.title') }}: Lixir E-Commerce Admin Dashboard
@endsection

@section('page_title')
    {{ __('lixir::system.tax-rate.edit.title') }}
@endsection

@section('content')
<a-row type="flex" justify="center">
    <a-col :span="24">
        <tax-rate-save
            base-url="{{ asset(config('lixir.admin_url')) }}"
            :tax-rate="{{ $taxRate }}" inline-template>
        <div>
            <a-form
                :form="taxRateForm"
                method="post"
                action="{{ route('admin.tax-rate.update', $taxRate->id) }}"
                @submit="handleSubmit"
            >
                @csrf
                @method('put')
                @include('lixir::system.tax-rate._fields')

                <a-form-item>
                    <a-button
                        type="primary"
                        html-type="submit"
                    >
                        {{ __('lixir::system.btn.save') }}
                    </a-button>

                    <a-button
                        class="ml-1"
                        type="default"
                        v-on:click.prevent="cancelTaxRate"
                    >
                        {{ __('lixir::system.btn.cancel') }}
                    </a-button>
                </a-form-item>
            </a-form>
            </div>
        </tax-rate-save>
    </a-col>
</a-row>
@endsection
