<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('meta_title', 'Lixir E-Commerce')</title>

    <!-- Styles -->
    <link href="{{ asset('lixir-admin/css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <lixir-layout inline-template >
            <a-layout id="lixir-admin-layout" style="min-height: 100vh">
                @include('lixir::partials.sidebar')
                <a-layout>

                    @include('lixir::partials.header')

                    @include('lixir::partials.flash')
                    @include('lixir::partials.breadcrumb')


                    <h1 class="mt-1 ml-1 mb-0">@yield('page_title')</h1>

                    <a-layout-content class="mh-1 ph-1 pt-1 bg-white">
                        @yield('content')
                    </a-layout-content>

                    @include('lixir::partials.footer')
                </a-layout>

            </a-layout>
        </lixir-layout>
    </div>
    <script src="{{ asset('lixir-admin/js/app.js') }}"></script>
    @stack('scripts')
</body>
</html>
