@extends('lixir::layouts.app')

@section('meta_title')
    {{ __('lixir::cms.menu.index.title') }}: Lixir E-Commerce Admin Dashboard
@endsection

@section('page_title')
    {{ __('lixir::cms.menu.index.title') }}
@endsection

@section('content')
<a-row type="flex" class="mb-1" justify="end">
    <a-col>
        <a
            href="{{ route('admin.menu-group.create') }}"
            class="ant-btn ant-btn-primary">
            <a-icon type="plus"></a-icon>
            {{ __('lixir::system.btn.create') }}
        </a>
    </a-col>
</a-row>
<a-row type="flex" justify="center">
    <a-col :span="24">
        <menu-table
            :menu-groups="{{ $menuGroups }}"
            inline-template base-url="{{ asset(config('lixir.admin_url')) }}">
            <a-table :columns="columns" row-key="id" :data-source="menuGroups" @change="handleTableChange">
                <span slot="action" slot-scope="text, record">

                    <a :href="getEditUrl(record)">
                        <a-icon type="edit"></a-icon>
                    </a>
                    <a :href="getDeleteUrl(record)" v-on:click.prevent="deleteMenuGroup(record)">
                        <a-icon type="delete"></a-icon>
                    </a>
                </span>
            </a-table>
        </menu-table>
    </a-col>
</a-row>
@endsection
