@component('mail::message')

@component('mail::panel')

Dear Customer,

# Lixir Shopping Cart Order Invoice

Thank you for shopping with Apple Sales New Zealand

Please find enclosed your official Tax Invoice.

Please retain a copy of this invoice for your records.

Thanks,

Lixir Shopping Cart Team
@endcomponent
