<?php

return [
    'admin_url' => 'admin',
    'symlink_storage_folder' => 'storage',
    'cart' => [
        'session_key' => 'cart_products',
        'promotion_key' => 'card_discount'
    ],
    'model' => [
        'user' => App\User::class
    ],
    'filesystems' => [
        'disks' => [
            'lixir' => [
                'driver' => 'local',
                'root' => storage_path('app/public')
            ]
        ]
    ],
    'image' => [
        'driver' => 'gd',
        'sizes' => [
            'small' => ['150', '150'],
            'med' => ['350', '350'],
            'large' => ['750', '750'],
        ],
    ],
    'auth' => [
        'guards' => [
            'admin' => [
                'driver' => 'session',
                'provider' => 'admin-users'
            ]
        ],
        'providers' => [
            'admin-users' => [
                'driver' => 'eloquent',
                'model' => Lixir\Database\Models\AdminUser::class
            ]
        ],
        'passwords' => [
            'adminusers' => [
                'providers' => 'admin-users',
                'table' => 'admin_password_resets',
                'expire' => 60
            ]
        ]
    ]
];
